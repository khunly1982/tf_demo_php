<?php
class Restaurant {
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $nom;

    /**
     * @var string
     */
    public $adresseRue;

     /**
     * @var string
     */
    public $adresseNum;

     /**
     * @var string
     */
    public $adresseCp;

     /**
     * @var string
     */
    public $adresseVille;

     /**
     * @var int
     */
    public $typeCuisineId;
}

?>

