<?php
// création de la classe
class Personne {

    // les attributs et les méthodes  sont appelés membres de classe
    // attributs de ma classe
    /**
     * @var string
     * Contient le nom de la personne
     */
    public $nom;

    /**
     * @var string
     */
    public $genre;

    /**
     * @var float
     */
    public $taille;

    // fonctions / methodes de la classe
    /**
     * affiche une phrase à l'écran
     */
    public function manger() {
        // instructions à éxécuter
        echo 'Je suis en train de manger';
    }
}

// instanciation d'une classe
$personne = new Personne();
// -> operateur de déreférencement // permet d'accéder aux membres de la classe
$personne->nom = 'Khun';
$personne->genre = 'Masculin';

echo $personne->nom;

$personne->manger();