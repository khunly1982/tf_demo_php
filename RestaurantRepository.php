<?php
include 'Restaurant.php';

class RestaurantRepository
{
    private $server = 'localhost';
    private $port = '3306';
    private $user = 'root';
    private $pwd = null;
    private $dbName = 'demo_php';

    // pour se connecter ([typePilote]:host=[nomDuServer];port=[port];dbname=[nomDB])
    private $pdo;

    // methode qui est appelée automatiquement à la création d'une instance
    public function __construct()
    {
        // permet d'initialiser les variables de la classe
        $this->pdo  = new PDO(
            'mysql:host=' . $this->server . ';port=' . $this->port . ';dbname=' . $this->dbName,
            $this->user,
            $this->pwd
        );
    }

    /**
     * Récupère touts les restaurants de la db
     */
    public function getAll()
    {
        // requête SQL
        $query = 'SELECT * FROM Restaurant';
        // preparation de la requete
        $stmt = $this->pdo->prepare($query);
        // envoi de la requete sur le serveur db
        $stmt->execute();
        // récupération du résulat
        $results = $stmt->fetchAll(PDO::FETCH_CLASS, Restaurant::class);
        if ($stmt->errorInfo()[2]) {
            echo $stmt->errorInfo()[2];
        }
        return $results;
    }

    /**
     * Récupère le resto dont l'id est celui passé en paramètre
     */
    public function getById($id)
    {
        $query = "SELECT * FROM Restaurant WHERE id = ?";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute([$id]);
        $result = $stmt->fetchObject(Restaurant::class);
        if ($stmt->errorInfo()[2]) {
            echo $stmt->errorInfo()[2];
        }
        return $result;
    }
}
