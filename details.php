<?php

// $_GET[] permet de récuréper les paramètres de la requêtes
$id = $_GET['id'];

include 'RestaurantRepository.php';

$repo = new RestaurantRepository();

$resto = $repo->getById($id);

include 'Views/detail_restaurant.php';